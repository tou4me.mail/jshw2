// ---------------------------Прямоугольник-------------------------------

for (i = 10; i > 0; i--) {
  for (q = 0; q < 10; q++) {
    document.write('&#9885;');
  }
  document.write('<br>');
}
document.write('<br>');
document.write('<br>');
document.write('<br>');
document.write('<br>');

// ---------------------------Прямоугольный треугольник-------------------

for (r = 1; r < 10; r++) {
  for (t = 1; t <= r; t++) {
    document.write('&#9885;');
  }
  document.write('<br>');
}
document.write('<br>');
document.write('<br>');
document.write('<br>');
document.write('<br>');

// ---------------------------Равносторонний треугольник-------------------

for (r = 1; r < 10; r++) {
  for (y = 10; y >= r; y--) {
    document.write('&nbsp;&nbsp;&nbsp;');
  }
  for (t = 1; t <= r; t++) {
    document.write('&#9885;');
  }
  for (u = 2; u <= r; u++) {
    document.write('&#9885;');
  }
  document.write('<br>');
}
document.write('<br>');
document.write('<br>');
document.write('<br>');
document.write('<br>');

// ---------------------------Ромб-------------------------------

for (r = 1; r < 10; r++) {
  for (y = 10; y >= r; y--) {
    document.write('&nbsp;&nbsp;&nbsp;');
  }
  for (t = 1; t <= r; t++) {
    document.write('&#9885;');
  }
  for (u = 2; u <= r; u++) {
    document.write('&#9885;');
  }
  document.write('<br>');
}
for (r = 10; r > 0; r--) {
  for (y = r; y <= 10; y++) {
    document.write('&nbsp;&nbsp;&nbsp;');
  }
  for (t = 1; t <= r; t++) {
    document.write('&#9885;');
  }
  for (u = 2; u <= r; u++) {
    document.write('&#9885;');
  }
  document.write('<br>');
}
